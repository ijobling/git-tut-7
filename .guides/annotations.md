@annotation:tour intro
#A POETIC INTRODUCTION TO GIT

#Module 7 - Undoing changes using 'git revert''

Whereas resetting is pretty easy to understand, reverting is a little harder on the brain initially. When I first saw ‘revert’ I thought it would ‘revert back to’ a specific commit, which is not the case. 

>`git revert` *undoes* or *reverts out* the changes introduced by one or more commits.

When you revert a commit (think when you 'undo the changes introduced by a commit') it does the following :

1. It figures out what file changes were introduced by the specified commit
1. It then creates a set of file changes the reverses out those file changes
1. It then creates a brand new commit snaphot and applies those 'reversing out' changes

Another important thing about `git revert` is that it is safe to use this command when operating on commits that are also present in remote repos. `git reset` is not safe this scenario.

@annotation:tour lastcommit
##Use Case : Reverting the most recent commit
The most common scenario is that you want to revert your last commit. Imagine you have the following set of commits

![](img/git-revert-init.png)

You suddenly decide that you want to get from your HEAD state (Commit 3) to Commit 2. In other words, you want to roll back the changes introduced by Commit 3.

The command you use for this is 

```
git revert HEAD
```

Think of this as reading “I want to revert out the changes I made in Commit 3 (which is the HEAD state) so I get back to the state I was in at Commit 2".

Unlike `git reset`, what Git does *NOT* do is to delete Commit 3. Instead, it analyses what needs to be done to reverse out the changes introduced by Commit 3 and then makes these changes as a brand new commit. 

As a result, your commit history will look like this.

![](img/git-revert-head.png)

As you can see, the entire commit history is intact and all that has happened is that there is a new commit with the Commit 3 changes reverted out. Your working directory will also be updated.

@annotation:tour severalcommits
#Use Case : Rolling back several commits
Let’s say you want to roll back several commits, not just the most recent one. In this case you can use a similar approach but merely specify the commit range. This is the way of thinking about specifying the range

```
git revert <last good commit>..<last bad commit>
```

This will revert changes starting with the <last bad commit> and working backwards until the one before the <last good commit>.

On your Codio Box, do the following

```
git revert HEAD~2..HEAD
```

This will revert out HEAD (Commit 3) and HEAD~1 (Commit 2) but *not* HEAD~2. 

Your commit history and your working directory will be updated with the necessary changes such that the changes introduced by the two most recent changes are removed.

You will end up in the same state as at Commit 1 *and* all of your commit history is fully intact.

![](img/git-revert-range.png)

You can also add `—no-commit` to the command, in which case no commit will be done but all the changes will be applied to your working directory and staged, so you have to commit manually later.

@annotation:tour furtherback
#Use Case : Undoing changes further back
It could be that you introduced a set of changes several commits ago that you want to revert out.

If you want to get rid of the changes you introduced in Commit 1, then you should think of this as ‘reverting out’ Commit 1. 

Use `git log` to find the sha id of Commit 1. Copy the first few characters to the clipboard.

Now enter

```
git revert sha-value
```

Your commit history and your working directory will be updated with the necessary changes such that the changes introduced by Commit 1 are reverted out. 

Your commit history and your working directory will be updated with the necessary changes such that the changes introduced by the specified commit are removed.

@annotation:tour checkoutcommit
#Checking out an earlier commit
You may find yourself wanting to inspect your project at a specific, earlier commit. By far the simplest way to do this is to use

```
git checkout sha-id
```

or maybe

```
git checkout HEAD^^^
```

**IMPORTANT** when you do this, your repo is in what is known as a ‘Detached State’. This means you should NOT make changes to your project without knowing what you are doing. 

If you try this out, run 

```
git status
```

and you will see the warning as follows

```
# HEAD detached at 525ecc4  
```

Generally speaking, using `git checkout` should be used for viewing only. Once you are done viewing, you will want to revert to the HEAD state using one of the following

```
git checkout branchname
```

which reverts to the HEAD state in the branch name you specify.

If you do decide to make changes when you are in a detached state, then you can recover things as follows

```
git checkout -b temp
```

This creates a new branch called `temp` and then immediately switches to it. All of your changes will be carried over to the new branch and the HEAD will be reset within that branch.

@annotation:tour play
#Playtime
Use the current project to play around with the variations of `git revert`. 


