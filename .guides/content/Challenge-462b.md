This is your first challenge.

Wait, what? It is too soon for a challenge!

The purpose of this challenge is to create some file versions in order for you to understand `git revert` better. After completing this challenge, we'll be looking at some use cases where you can practice the theory that is being exposed.

By the end of this challenge, doing a `git log -3` should look like this (commit meta-info may vary): 

```bash
commit dyuh47d99cadd39ee1560ea839dd8018c9cb4592
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:44 2015 +0100

    Added file 3

commit dyuh47d3be1949c4315d0745b99ea4ae44032647
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:30 2015 +0100

    Added file 2

commit dyuh47df20f1b66cd7641dbc8089e4231c966b06
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:04 2015 +0100

    Added file 1
```

While the file tree should end up with 4 files inside the `revert` folder: 

```bash
revert/
- file-1.txt
- file-2.txt
- file-3.txt
- log.txt
```

The `log.txt` file is already created for you and by the end of the challenge its content should look like this: 

```bash
Added file 1
Added file 2
Added file 3
```

Time to complete the challenge. 

{Check It!|assessment}(test-2779582619)

|||guidance

### Correct answers: 

1. Create the `file-1.txt` file in the `revert` folder
2. Add the "Added file 1" message in the `log.txt` file
3. Commit the changes adding the message: "Added file 1"
1. Create the `file-2.txt` file in the `revert` folder
2. Add the "Added file 2" message in the `log.txt` file
3. Commit the changes adding the message: "Added file 2"
1. Create the `file-3.txt` file in the `revert` folder
2. Add the "Added file 3" message in the `log.txt` file
3. Commit the changes adding the message: "Added file 3"


|||