This challenge relies on the file tree and commits structure built on section 2.1 challenge.

By the end of this challenge, executing a `git log -3` should output this (commit meta-info may vary):

```bash
commit c5e56cbd442f7c4cd1a12dcfabdd312e711488ca
Author: user <user@email.com>
Date:   Wed Nov 11 13:21:04 2015 +0100

    Revert "Added file 2"

    This reverts commit d40e7579203d5e6e9ecde77716c5272b2474de1b.

commit d40e7579203d5e6e9ecde77716c5272b2474de1b
Author: user <user@email.com>
Date:   Tue Nov 10 21:47:45 2015 +0100

    Added file 2

commit 16c0dcb731eca2faad8e8e6b516aade29146ed21
Author: user <user@email.com>
Date:   Tue Nov 10 21:47:23 2015 +0100

    Added file 1
```

And the `revert` folder of your file tree should look like this: 

```bash
revert/
- file-1.txt
- log.txt
```

The `log.txt` file content should be this one:

```bash
Added file 1
```

{Check It!|assessment}(test-1736742236)

|||guidance

### Correct answers:

1. `git reset --hard <sha-id>` 
Where `<sha-id>` is the user's commit got from doing a `git log -6 --grep="Added file 2"` 
2. `git revert HEAD`

|||