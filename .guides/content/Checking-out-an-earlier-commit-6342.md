You may find yourself wanting to inspect your project at a specific, earlier commit. 

By far the simplest way to do this is to use:

```bash
git checkout <sha-id>
```

or maybe

```bash
git checkout HEAD^
```

**IMPORTANT** when you do this, your repo is in what is known as a ‘Detached State’. This means you should NOT make changes to your project without knowing what you are doing. 

If you try this out, run 

```bash
git status
```

and you will see the warning as follows

```bash
# HEAD detached at 525ecc4  
```

Generally speaking, using `git checkout` should be used for viewing only. Once you are done viewing, you will want to revert to the HEAD state using one of the following

```bash
git checkout current_branchname
```

which reverts to the `HEAD` state in the branch name you specify, in this case `master` is the only and current branch.

If you do decide to make changes when you are in a detached state, then you can recover things as follows

```bash
git checkout -b temp
```

This creates a new branch called `temp` and then immediately switches to it. All of your changes will be carried over to the new branch and the `HEAD` will be reset within that branch.

---
Let's try this theory out with the _Added file 2_ commit sha-id in the next section.