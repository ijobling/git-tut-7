You'll come across the `You are in 'detached HEAD' state.` message and doing a `git log` will output _Added file 2_ as `HEAD`, however, it is not the definite `HEAD` yet. If you do a `git log --all` you'll see the real `HEAD`: _Revert "Added file 2"_. 

Do a `git branch` to see the only branch `master` and then do a: `git checkout -b temp` to create a new `temp` branch and make _Added file 2_ the new `HEAD` state.

Do a `git branch` again and you'll see this: 

```bash
master
* temp
```

Now you can play with that new branch and its files, update them as you wish and merge them to master afterwards. 