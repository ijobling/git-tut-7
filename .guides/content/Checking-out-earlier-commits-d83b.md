This section relies on the file tree and commits structure built on the previous challenge.

Executing a `git log -3` should output this (commit meta-info may vary):

```bash
commit c5e56cbd442f7c4cd1a12dcfabdd312e711488ca
Author: user <user@email.com>
Date:   Wed Nov 11 13:21:04 2015 +0100

    Revert "Added file 2"

    This reverts commit d40e7579203d5e6e9ecde77716c5272b2474de1b.

commit d40e7579203d5e6e9ecde77716c5272b2474de1b
Author: user <user@email.com>
Date:   Tue Nov 10 21:47:45 2015 +0100

    Added file 2

commit 16c0dcb731eca2faad8e8e6b516aade29146ed21
Author: user <user@email.com>
Date:   Tue Nov 10 21:47:23 2015 +0100

    Added file 1
```

Let's use `git checkout <sha-id>` where `<sha-id>` is the original _Added file 2_ commit sha-id.

---
Continue in the next section.