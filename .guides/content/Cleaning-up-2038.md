If you followed along with the previous sections, by the time of this section your commit history should look pretty messy.

Let's do a `git reset --hard <sha-id>` to get back to the original _Added file 3_ commit state.

To find out the proper commit _sha-id_, type in your terminal window:

```bash
git log -8 --grep="Added file 3"
```

And copy the first 5 or 7 digits of the sha-id of the original _Added file 3_ commit (should be the first one from bottom to top in the output).

After executing the `git reset --hard <sha-id>` command, your file tree and your commit history should get back to the state after the _setup challenge_.

---
Continue in the next section.