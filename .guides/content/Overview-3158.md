## Undoing changes using 'git revert'

Whereas resetting is pretty easy to understand, reverting is a little harder on the brain initially. When I first saw ‘revert’ I thought it would ‘revert back to’ a specific commit, which is not the case. 

>`git revert` *undoes* or *reverts out* the changes introduced by one or more commits.

When you revert a commit (think when you 'undo the changes introduced by a commit') it does the following :

1. It figures out what file changes were introduced by the specified commit
1. It then creates a set of file changes then reverses out those file changes
1. It then creates a brand new commit snaphot and applies those 'reversing out' changes

Another important thing about `git revert` is that it is safe to use this command when operating on commits that are also present in remote repos. `git reset` is not safe this scenario.