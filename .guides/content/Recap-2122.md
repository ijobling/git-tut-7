In the previous unit, you experimented with `git reset`. 

`git reset` is a very powerful command to help you keep your project versions neat, or very dangerous if you don't know what you are doing. 

By allowing you to _go back_ to previous commits in your commit history, `git reset` will set `HEAD` in the commit you specify and delete all the commits from that point in time. 

However, remember that you can choose whether to keep or delete the files in the working directory.

