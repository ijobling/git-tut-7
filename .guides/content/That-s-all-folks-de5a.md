All right! We are done with the introduction to Git. 

If you have got this far, then we are confident that you know enough Git in order to:

- _Version control_ your projects
- Collaborate in open source projects with developers all around the world
- Keep consistent backups of your projects
- Feel more comfortable with the command line

### Where to go from here?

- Checkout the official [Git reference](https://git-scm.com/docs)
- Scroll through the [most popular open source projects](https://github.com/trending?since=weekly) on GitHub every week
- If you like client UI's then feel free to download the [GitHub desktop client](https://desktop.github.com/)