This will revert all the changes made to files in working directory back until _Added file 1_, you can check the `log.txt` file to confirm the content update.

Your commit history and your working directory will be updated with the necessary changes such that the changes introduced by the 3 most recent changes are removed.

You will end up in the same state as at _Added file 1_ *and* all of your commit history is fully intact.

Try to analyse a `git log` now. It will list all the automatic revert commit messages.

---

You can also add `—no-commit` to the command, in which case no commit will be done but all the changes will be applied to your working directory and staged, so you have to commit manually later.
