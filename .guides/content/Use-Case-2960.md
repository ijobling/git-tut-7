### Rolling back several commits

Let’s say you want to roll back several commits, not just the most recent one. In this case you can use a similar approach but merely specify the commit range. This is the way of thinking about specifying the range

```bash
git revert <last good commit>..<last bad commit>
```

This will revert changes starting with the `<last bad commit>` and working backwards until the one before the `<last good commit>`.

Let's revert back to _Added file 1_, which is the same as `HEAD~3` because we are going 4 commits back in time from `HEAD`, which is currently _Revert "Added file 3"_.

On the terminal window, do the following:

```bash
git revert HEAD~3..HEAD
```

|||warning

### Nano x3
This time, the nano editor will prompt for confirmation 3 times, execute the `ctrl + x` command accordingly.

|||

---
Continue in the next section.