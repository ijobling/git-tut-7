|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file revert/file-1.txt revert/file-2.txt revert/file-3.txt revert/log.txt)

|||


This use case relies on the file tree and commits structure built on the previous challenge.

### Reverting the most recent commit

The most common scenario is that you want to revert your last commit. Do a `git log -3` to see this:

```bash
commit dyuh47d99cadd39ee1560ea839dd8018c9cb4592
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:44 2015 +0100

    Added file 3

commit dyuh47d3be1949c4315d0745b99ea4ae44032647
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:30 2015 +0100

    Added file 2

commit dyuh47df20f1b66cd7641dbc8089e4231c966b06
Author: user <user@email.com>
Date:   Tue Nov 10 12:18:04 2015 +0100

    Added file 1
```

You suddenly decide that you want to get from your `HEAD` state (Added file 3) to `HEAD^` (Added file 2). In other words, you want to roll back the changes introduced by `HEAD`.

The command you use for this is 

```bash
git revert HEAD
```

|||warning

### Welcome to the _nano_ window

After executing the `git revert HEAD` command, you'll come across this terminal screen:

![nano](.guides/img/nano-1.png)

This is the _nano_ editor prompting you for confirmation. In order to confirm just press `ctrl + x` as stated in the nano editor footer (`^X Exit`): 

![nano-footer](.guides/img/nano-footer.png)

After doing this, just press the letter _n_ key in your keyboard to exit the editor.

|||

---
Continue in the next section.