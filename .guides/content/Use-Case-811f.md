### Undoing changes further back

It could be that you introduced a set of changes several commits ago that you want to revert out.

If you want to get rid of the changes you introduced in _Added file 1_, then you should think of this as ‘reverting out’ _Added file 1_ `sha-id`.

Use `git log -4 --grep="Added file 1"` to find the sha-id of _Added file 1_. Copy the first few characters to the clipboard.

Now enter:

```bash
git revert <sha-id>
```

After having executed this command, you'll get an error like this: 

```bash
error: could not revert 1234abc... Added file 1
hint: after resolving the conflicts, mark the corrected paths
hint: with 'git add <paths>' or 'git rm <paths>'
hint: and commit the result with 'git commit'
```

So, what happened and how to solve it?

---
Continue in the next section.