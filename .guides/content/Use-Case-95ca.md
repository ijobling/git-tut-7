Think of this as reading “I want to revert out the changes I made in _Added file 3_ (which is the `HEAD` state) so I get back to the state I was in at _Added file 2_".

Unlike `git reset`, what Git does *NOT* do is to delete _Added file 3_. Instead, it analyses what needs to be done to reverse out the changes introduced by _Added file 3_ and then makes these changes as a brand new commit. 

As a result, your commit history will look like this (execute a `git log -4`):

```bash

commit e2f22864b6a732db7c01565e124b6dd09bd6bb7e
Author: user <user@email.com>
Date:   Tue Nov 10 20:44:54 2015 +0100

    Revert "Added file 3"

    This reverts commit 206e129425b8c5d5b617a50c9a973d06e05f57cf.

commit 206e129425b8c5d5b617a50c9a973d06e05f57cf
Author: user <user@email.com>
Date:   Tue Nov 10 20:44:22 2015 +0100

    Added file 3

...

```

As you can see, the entire commit history is intact and all that has happened is that there is a new commit with the _Added file 3_ changes reverted out. Your working directory will also be updated.

You can also take a look at the `log.txt` file and confirm that the _Added file 3_ message was removed, as well as the `file-3.txt` from the file tree.