By reverting commits by specific sha-id's, Git tries its best to get the project state  as it was in that specified commit, however, sometimes it can't. 

Take a look at your file tree, the `revert/file-1.txt` is gone while the files from earlier commits are still there (`file-2.txt`, `file-3.txt` and `log.txt`) this is what we mean by _reverting out_. 

Take a look at `log.txt`. The file should only have the _Added file 1_ message but it has more than that. Git is asking you to confirm that this is the proper state of the file and then commit the update.

Your commit history and your working directory will be updated with the necessary changes such that the changes introduced by _Added file 1_ are reverted out.