#!/bin/bash
QCOUNT=9

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Create the file-1.txt in the revert folder" "file-1.txt" "ls /home/codio/workspace/revert"
				;;
      2 )
				expect "Create the file-2.txt in the revert folder" "file-2.txt" "ls /home/codio/workspace/revert"
				;;
      3 )
				expect "Create the file-3.txt in the revert folder" "file-3.txt" "ls /home/codio/workspace/revert"
				;;
      4 )
				expect "Commit the changes using the proper message" "Added[[:space:]]*file[[:space:]]*3" "git log -3"
				;;
      5 )
				expect "Commit the changes using the proper message" "Added[[:space:]]*file[[:space:]]*2" "git log -3"
				;;
			6 )
				expect "Commit the changes using the proper message" "Added[[:space:]]*file[[:space:]]*1" "git log -3"
				;;
      7 )
        expect "log.txt has not the right messages format" "Added[[:space:]]*file[[:space:]]*3" "grep 3 /home/codio/workspace/revert/log.txt"
        ;;
      8 )
        expect "log.txt has not the right messages format" "Added[[:space:]]*file[[:space:]]*2" "grep 2 /home/codio/workspace/revert/log.txt"
        ;;
      9 )
        expect "log.txt has not the right messages format" "Added[[:space:]]*file[[:space:]]*1" "grep 1 /home/codio/workspace/revert/log.txt"
        ;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command