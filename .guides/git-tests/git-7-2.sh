#!/bin/bash
QCOUNT=5

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Inexistent file: revert/file-1.txt" "file-1.txt" "ls /home/codio/workspace/revert"
				;;
      2 )
				expect "Reset and revert the project accordingly" "Revert[[:space:]]*\"Added[[:space:]]*file[[:space:]]*2\"" "git log -3"
				;;
      3 )
				expect "Reset and revert the project accordingly" "Added[[:space:]]*file[[:space:]]*2" "git log -3"
				;;
			4 )
				expect "Reset and revert the project accordingly" "Added[[:space:]]*file[[:space:]]*1" "git log -3"
				;;
      5 )
        expect "log.txt has not the right messages format" "Added[[:space:]]*file[[:space:]]*1" "grep 1 /home/codio/workspace/revert/log.txt"
        ;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command